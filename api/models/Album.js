const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    date: String,
    is_published: {
        type: Boolean,
        default: false
    },
    image: String
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;

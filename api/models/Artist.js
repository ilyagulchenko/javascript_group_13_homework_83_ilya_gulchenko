const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    information: String,
    is_published: {
        type: Boolean,
        default: false
    },
    image: String
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;

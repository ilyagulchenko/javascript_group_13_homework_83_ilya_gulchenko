const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    is_published: {
        type: Boolean,
        default: false
    },
    length: String
})

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;

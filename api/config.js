const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongoConfig: {
        db: 'mongodb://localhost/artists',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '502270274714917',
        appSecret: '1d1232a16a0cfca312c7914e00775fef'
    }
}

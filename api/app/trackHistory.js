const express = require('express');
const TrackHistory = require('../models/TrackHistory')
const Track = require("../models/Track");
const auth = require("../middleware/auth");
const Album = require("../models/Album");

const router = express.Router();

router.post('/', auth, async (req,res,next) => {
    try {
        const trackHistoryData = {
            user: req.user,
            track: await Track.findById(req.body.id.id),
            datetime: new Date().toLocaleString()
        };

        const trackHistory = new TrackHistory(trackHistoryData);

        await trackHistory.save();

        return res.send(trackHistory);
    } catch (e) {
        next(e);
    }
});

router.get('/', async (req, res, next) => {
    try {
        const sort = {};
        let data = [];

        const fields = [
            {
                path:'user',
                select:'displayName'
            },
            {
                path:'track',
                select:'title length album'
            },
        ];

        sort._id = -1;

        const trackHistory = await TrackHistory.find().sort(sort).populate(fields);
        const albums = await Album.find().populate('artist', 'name');

        for (let value of trackHistory) {
            for (let another of albums) {
                if (another._id.toString() === value.track.album.toString()) {
                    let obj = {
                        user: value.user,
                        track: value.track,
                        artist: another.artist,
                        datetime: value.datetime
                    }
                    data.push(obj);
                }
            }
        }

        return res.send(data);
    } catch (e) {
        next(e);
    }
})

module.exports = router;

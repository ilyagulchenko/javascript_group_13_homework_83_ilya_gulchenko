const express = require('express');
const Track = require('../models/Track')
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', async (req,res,next) => {
    try {
        const query = {};

        if (req.query.album) {
            query.album = req.query.album;
        }

        const tracks = await Track.find(query).populate("album", "title");

        return res.send(tracks);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, async (req, res, next) => {
    try {
        const trackData = {
            album: req.body.album,
            title: req.body.title,
            length: req.body.length,
        };

        if (req.user.role === 'admin') {
            trackData.is_published = true;
        }

        const track = new Track(trackData);

        await track.save();

        return res.send(track);
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const track = await Track.findById(req.params.id);

        if (!track) {
            return res.status(404).send({error: 'Track not found'});
        }

        track.is_published = true;

        await track.save();

        return res.send(track);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(error);
        }

        return next(error);
    }
});

router.delete("/:id", auth, permit('admin'), async (req, res, next) => {
    try {
        await Track.deleteOne({_id: req.params.id});

        return res.send({message: 'Track deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;

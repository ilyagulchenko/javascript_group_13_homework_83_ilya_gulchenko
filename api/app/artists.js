const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        const query = {};

        if (req.query.filter === 'image') {
            query.image = {$exists: true};
        }

        if (req.query.id) {
            query.artist = req.query.artist;
        }

        const artists = await Artist.find(query);

        return res.send(artists);
    } catch (e) {
        next(e);
    }
});

router.get("/:id", async (req, res, next) => {
    try {
        const artist = await Artist.findById(req.params.id);

        if (!artist) {
            return res.status(404).send({message: 'Not found!'});
        }

        return res.send(artist);
    } catch (e) {
        next(e);
    }
});

router.post("/", auth, upload.single('image'), async (req, res, next) => {
    try {
        const artistData = {
            name: req.body.name,
            information: req.body.information,
            image: null,
        };

        if (req.file) {
            artistData.image = req.file.filename;
        }

        if (req.user.role === 'admin') {
            artistData.is_published = true;
        }

        const artist = new Artist(artistData);

        await artist.save();

        return res.send(artist);
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const artist = await Artist.findById(req.params.id);

        if (!artist) {
            return res.status(404).send({error: 'Artist not found'});
        }

        artist.is_published = true;

        await artist.save();

        return res.send(artist);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(error);
        }

        return next(error);
    }
});

router.delete("/:id", auth, permit('admin'), async (req, res, next) => {
    try {
        await Artist.deleteOne({_id: req.params.id});

        return res.send({message: 'Artist deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;

const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require("../config");
const Album = require("../models/Album");
const auth = require("../middleware/auth");
const Artist = require("../models/Artist");
const permit = require("../middleware/permit");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req,res,next) => {
    try {
        const query = {};
        const sort = {};

        if (req.query.filter === 'image') {
            query.image = {$exists: true};
        }

        if (req.query.artist) {
            query.artist = req.query.artist;
        }

        const data = {
            albums: await Album.find(query).sort(sort).populate("artist", "name"),
            artist: await Artist.findById(req.query.artist)
        }

        return res.send(data);
    } catch (e) {
        next(e);
    }
});

router.get('/:id',async (req,res, next) => {
    try {
        const album = await Album.findById(req.params.id);

        if (!album) {
            return res.status(404).send({message: 'Not found!'});
        }

        return res.send(album);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
    try {
        const albumData = {
            artist: req.body.artist,
            title: req.body.title,
            date: req.body.date,
            image: null,
        };

        if (req.file) {
            albumData.image = req.file.filename;
        }

        if (req.user.role === 'admin') {
            albumData.is_published = true;
        }

        const album = new Album(albumData);

        await album.save();

        return res.send(album);
    } catch (e) {
        next(e);
    }
});

router.delete("/:id", auth, permit('admin'), async (req, res, next) => {
    try {
        await Album.deleteOne({_id: req.params.id});

        return res.send({message: 'Album deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;

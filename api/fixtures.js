const mongoose = require('mongoose');
const config = require('./config');
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const {nanoid} = require("nanoid");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [artistOne, artistTwo] = await Artist.create({
        name: 'Земфира',
        information: 'Российская рок-певица, музыкант, композитор, продюсер, поэт и автор песен.',
        is_published: true,
        image: 'Zemfira.jpg'
    }, {
        name: 'Пицца',
        information: 'Музыкальная группа, основанная в 2010 году Сергеем Приказчиковым, который является автором, композитором и аранжировщиком песен группы.',
        is_published: false,
        image: 'Pizza.jpg'
    });

    const [albumOne, albumTwo, albumThree] = await Album.create({
        title: 'Прости меня моя любовь',
        date: '12.01.2000',
        is_published: true,
        image: 'Prosti.jpg',
        artist: artistOne,
    }, {
        title: 'Жить в твоей голове',
        date: '29.01.2012',
        is_published: false,
        image: 'Zhit.jpg',
        artist: artistOne,
    }, {
        title: 'Кухня',
        date: '26.07.2012',
        is_published: true,
        image: 'Kuhnya.jpg',
        artist: artistTwo,
    });

    await Track.create({
        title: 'п.м.м.л.',
        length: '3:36',
        album: albumOne,
    }, {
        title: 'Искала',
        length: '3:34',
        album: albumOne,
    }, {
        title: 'Сигареты',
        length: '3:12',
        album: albumOne,
    }, {
        title: 'Не отпускай',
        length: '3:26',
        album: albumOne,
    }, {
        title: 'Ненавижу',
        length: '3:44',
        album: albumOne,
    }, {
        title: 'Жить в твоей голове',
        length: '5:20',
        album: albumTwo,
    }, {
        title: 'Без шансов',
        length: '2:44',
        album: albumTwo,
    }, {
        title: 'Деньги',
        length: '3:18',
        album: albumTwo,
    }, {
        title: 'Река',
        length: '4:01',
        album: albumTwo,
    }, {
        title: 'Если бы',
        length: '3:55',
        album: albumTwo,
    }, {
        title: 'Фары',
        length: '4:46',
        album: albumThree,
    }, {
        title: 'Пятница',
        length: '3:06',
        album: albumThree,
    }, {
        title: 'Оружие',
        length: '3:27',
        album: albumThree,
    }, {
        title: 'Она',
        length: '3:01',
        album: albumThree,
    }, {
        title: 'Париж',
        length: '3:52',
        album: albumThree,
    });

    await User.create({
        email: 'user@shop.com',
        password: '12345',
        token: nanoid(),
        role: 'user',
        displayName: 'User',
        avatar: 'user.png'
    }, {
        email: 'admin@shop.com',
        password: '12345',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin',
        avatar: 'admin.png'
    })

    await mongoose.connection.close();
};

run().catch(e => console.error(e));

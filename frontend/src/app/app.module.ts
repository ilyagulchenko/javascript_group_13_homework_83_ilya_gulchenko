import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerformersComponent } from './pages/performers/performers.component';
import { PerformerDetailsComponent } from './pages/performers/performer-details/performer-details.component';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImagePipe } from './pipes/image.pipe';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { RegisterComponent } from './register/register.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { ValidateIdenticalDirective } from './validate-identical.directive';
import { LoginComponent } from './login/login.component';
import { MatMenuModule } from '@angular/material/menu';
import { PerformerTracksComponent } from './pages/performers/performer-tracks/performer-tracks.component';
import {
  PerformerTrackHistoryComponent
} from './pages/performers/performer-track-history/performer-track-history.component';
import { AppStoreModule } from './store/app-store.module';
import { EditArtistComponent } from './pages/edit-artist/edit-artist.component';
import { EditAlbumComponent } from './pages/edit-album/edit-album.component';
import { EditTrackComponent } from './pages/edit-track/edit-track.component';
import { MatSelectModule } from '@angular/material/select';
import { AuthInterceptor } from './auth.interceptor';
import { HasRolesDirective } from './directives/has-roles.directive';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppId, {
        scope: 'email,public_profile',
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    PerformersComponent,
    PerformerDetailsComponent,
    ImagePipe,
    RegisterComponent,
    FileInputComponent,
    ValidateIdenticalDirective,
    LoginComponent,
    PerformerTracksComponent,
    PerformerTrackHistoryComponent,
    EditArtistComponent,
    EditAlbumComponent,
    EditTrackComponent,
    HasRolesDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    FlexModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    AppStoreModule,
    MatSelectModule,
    SocialLoginModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: 'SocialAuthServiceConfig', useValue: socialConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AlbumData, AlbumToCreate} from '../models/artist.model';
import {environment as env} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(private http: HttpClient) {}

  getAlbums() {
    return this.http.get<AlbumToCreate>(env.apiUrl + '/albums');
  }

  createAlbum(albumData: AlbumData) {
    const formData = new FormData();

    Object.keys(albumData).forEach(key => {
      if (albumData[key] !== null) {
        formData.append(key, albumData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/albums', formData);
  }

  removeAlbum(id: string) {
    return this.http.delete(env.apiUrl + `/albums/${id}`);
  }
}

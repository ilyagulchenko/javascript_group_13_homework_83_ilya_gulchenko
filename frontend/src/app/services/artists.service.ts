import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiArtistData, Artist, ArtistData, DataApi} from '../models/artist.model';
import {environment as env} from '../../environments/environment';
import {map} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArtistsService {

  constructor(private http: HttpClient) {}

  getArtists() {
    return this.http.get<ApiArtistData[]>(env.apiUrl + '/artists').pipe(
      map(response => {
        return response.map(artistData => {
          return new Artist(
            artistData._id,
            artistData.name,
            artistData.information,
            artistData.is_published,
            artistData.image,
          );
        });
      })
    )
  }

  getArtistById(id: string) {
    return this.http.get<DataApi>(env.apiUrl + `/albums?artist=${id}`);
  }

  createArtist(artistData: ArtistData) {
    const formData = new FormData();

    Object.keys(artistData).forEach(key => {
      if (artistData[key] !== null) {
        formData.append(key, artistData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/artists', formData);
  }

  publishArtist(id: string, change: {is_published: true}) {
    return this.http.put<Artist>(env.apiUrl + '/artists/' + id, change);
  }

  removeArtist(id: string) {
    return this.http.delete(env.apiUrl + `/artists/${id}`);
  }

}

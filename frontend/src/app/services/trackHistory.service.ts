import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TrackHistory, TrackHistoryData} from '../models/trackHistory.model';
import {map} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrackHistoryService {

  constructor(private http: HttpClient) {}

  fetchTrackHistory() {
    return this.http.get<TrackHistoryData[]>(environment.apiUrl + '/track_history').pipe(
      map(response => {
        return response.map(trackHistoryData => {
          return new TrackHistory(
            trackHistoryData.user,
            trackHistoryData.track,
            trackHistoryData.artist,
            trackHistoryData.datetime,
          );
        });
      })
    )
  }

  addTrackToHistory(id: { id: any }, token: string) {
    return this.http.post(environment.apiUrl + '/track_history', id , {
      headers: new HttpHeaders({'Authorization' : token})
    });
  }
}

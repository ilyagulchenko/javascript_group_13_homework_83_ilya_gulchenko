import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment as env, environment} from '../../environments/environment';
import {Track, TrackData} from '../models/track.model';

@Injectable({
  providedIn: 'root'
})
export class TracksService {

  constructor(private http: HttpClient) {}

  getTracks(id: string) {
    return this.http.get<Track[]>(environment.apiUrl + `/tracks?album=${id}`);
  }

  createTrack(trackData: TrackData) {
    return this.http.post(env.apiUrl + '/tracks', trackData);
  }

  publishTrack(id: string, change: {is_published: true}) {
    return this.http.put<Track>(env.apiUrl + '/tracks/' + id, change);
  }

  removeTrack(id: string) {
    return this.http.delete(env.apiUrl + `/tracks/${id}`);
  }
}

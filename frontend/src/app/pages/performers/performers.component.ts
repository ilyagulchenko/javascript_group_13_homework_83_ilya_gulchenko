import { Component, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Artist } from '../../models/artist.model';
import {changeArtistRequest, fetchArtistsRequest, removeArtistRequest} from '../../store/artists/artists.actions';

@Component({
  selector: 'app-performers',
  templateUrl: './performers.component.html',
  styleUrls: ['./performers.component.css']
})
export class PerformersComponent implements OnInit {
  artists: Observable<Artist[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
    this.loading = store.select(state => state.artists.fetchLoading);
    this.error = store.select(state => state.artists.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  publishArtist(id: string, change: {is_published: true}) {
    this.store.dispatch(changeArtistRequest({id, change}));
  }

  removeArtist(id: string) {
    this.store.dispatch(removeArtistRequest({id}));
    void this.ngOnInit();
  }

}

import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TrackHistory} from '../../../models/trackHistory.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/types';
import {fetchTrackHistoryRequest} from '../../../store/track-histories/trackHistory.actions';

@Component({
  selector: 'app-performer-track-history',
  templateUrl: './performer-track-history.component.html',
  styleUrls: ['./performer-track-history.component.css']
})
export class PerformerTrackHistoryComponent implements OnInit {
  trackHistory: Observable<TrackHistory[]>;

  constructor(private store: Store<AppState>) {
    this.trackHistory = store.select(state => state.trackHistory.trackHistory);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTrackHistoryRequest());
  }

}

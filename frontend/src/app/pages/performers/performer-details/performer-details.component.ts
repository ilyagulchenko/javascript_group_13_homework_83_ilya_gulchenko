import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {AppState} from '../../../store/types';
import {fetchDataRequest} from '../../../store/data.actions';
import {removeAlbumRequest} from '../../../store/albums/albums.actions';

@Component({
  selector: 'app-performer-details',
  templateUrl: './performer-details.component.html',
  styleUrls: ['./performer-details.component.css']
})
export class PerformerDetailsComponent {
  data: Observable<any>;
  loading: Observable<boolean>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.data = store.select(state => state.data.data);
    this.loading = store.select(state => state.data.fetchLoading);
  }

  ngOnInit(): void {
    // @ts-ignore
    const id: string = this.route.params.value.id
    this.store.dispatch(fetchDataRequest({id}));
  }

  removeAlbum(id: string) {
    this.store.dispatch(removeAlbumRequest({id}));
    this.ngOnInit();
  }

}

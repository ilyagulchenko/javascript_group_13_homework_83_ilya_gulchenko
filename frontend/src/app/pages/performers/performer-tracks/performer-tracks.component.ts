import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Track} from '../../../models/track.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/types';
import {ActivatedRoute} from '@angular/router';
import {changeTrackRequest, fetchTracksRequest, removeTrackRequest} from '../../../store/tracks/tracks.actions';
import {User} from '../../../models/user.model';
import {createTrackHistoryRequest} from '../../../store/track-histories/trackHistory.actions';

@Component({
  selector: 'app-performer-tracks',
  templateUrl: './performer-tracks.component.html',
  styleUrls: ['./performer-tracks.component.css']
})
export class PerformerTracksComponent implements OnInit {
  tracks: Observable<Track[]>;
  loading: Observable<boolean>;
  user: Observable<null | User>

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    // @ts-ignore
    const id: string = this.route.params.value.id
    this.store.dispatch(fetchTracksRequest({id}));
  }

  addToHistory(id: string) {
    this.store.dispatch(createTrackHistoryRequest({id}))
  }

  publishTrack(id: string, change: {is_published: true}) {
    this.store.dispatch(changeTrackRequest({id, change}));
  }

  removeTrack(id: string) {
    this.store.dispatch(removeTrackRequest({id}));
    this.ngOnInit();
  }

}

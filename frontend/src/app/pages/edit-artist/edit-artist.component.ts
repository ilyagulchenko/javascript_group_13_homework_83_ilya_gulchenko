import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/types';
import {ArtistData} from '../../models/artist.model';
import {createArtistRequest} from '../../store/artists/artists.actions';

@Component({
  selector: 'app-edit-artist',
  templateUrl: './edit-artist.component.html',
  styleUrls: ['./edit-artist.component.css']
})
export class EditArtistComponent {
  @ViewChild('f') form!: NgForm;

  constructor(private store: Store<AppState>) {}

  createArtist() {
    const artistData: ArtistData = this.form.value;
    this.store.dispatch(createArtistRequest({artistData}));
  }

}

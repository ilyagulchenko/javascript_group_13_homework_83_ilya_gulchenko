import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {AlbumData, Artist} from '../../models/artist.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/types';
import {fetchArtistsRequest} from '../../store/artists/artists.actions';
import {NgForm} from '@angular/forms';
import {createAlbumRequest} from '../../store/albums/albums.actions';

@Component({
  selector: 'app-edit-album',
  templateUrl: './edit-album.component.html',
  styleUrls: ['./edit-album.component.css']
})
export class EditAlbumComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  artists: Observable<Artist[]>;

  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  createAlbum() {
    const albumData: AlbumData = this.form.value;
    console.log(albumData);
    this.store.dispatch(createAlbumRequest({albumData}));
  }

}

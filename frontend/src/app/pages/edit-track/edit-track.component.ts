import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/types';
import {AlbumToCreate} from '../../models/artist.model';
import {TrackData} from '../../models/track.model';
import {createTrackRequest} from '../../store/tracks/tracks.actions';
import {Observable} from 'rxjs';
import {fetchAlbumsRequest} from '../../store/albums/albums.actions';

@Component({
  selector: 'app-edit-track',
  templateUrl: './edit-track.component.html',
  styleUrls: ['./edit-track.component.css']
})
export class EditTrackComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  albums: Observable<null | AlbumToCreate>;

  constructor(private store: Store<AppState>) {
    this.albums = store.select(state => state.albums.albums);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchAlbumsRequest());
  }

  createTrack() {
    const trackData: TrackData = this.form.value;
    console.log(trackData);
    this.store.dispatch(createTrackRequest({trackData}));
  }

}

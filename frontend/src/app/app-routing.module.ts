import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PerformersComponent } from './pages/performers/performers.component';
import { PerformerDetailsComponent } from './pages/performers/performer-details/performer-details.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { PerformerTracksComponent } from './pages/performers/performer-tracks/performer-tracks.component';
import {PerformerTrackHistoryComponent} from './pages/performers/performer-track-history/performer-track-history.component';
import {EditArtistComponent} from './pages/edit-artist/edit-artist.component';
import {EditAlbumComponent} from './pages/edit-album/edit-album.component';
import {EditTrackComponent} from './pages/edit-track/edit-track.component';

const routes: Routes = [
  {path: '', component: PerformersComponent},
  {path: 'trackHistory', component: PerformerTrackHistoryComponent},
  {path: 'artist/new', component: EditArtistComponent},
  {path: 'album/new', component: EditAlbumComponent},
  {path: 'track/new', component: EditTrackComponent},
  {path: 'artist/:id', component: PerformerDetailsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: ':id', component: PerformerTracksComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

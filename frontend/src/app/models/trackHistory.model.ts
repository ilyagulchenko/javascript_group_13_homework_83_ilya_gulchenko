export class TrackHistory {
  constructor(
    public user: {
      _id: string,
      displayName: string,
    },
    public track: {
      _id: string,
      title: string,
      length: string,
      album: string
    },
    public artist: {
      _id: string,
      name: string
    },
    public datetime: string
  ) {}
}

export interface TrackHistoryData {
  user: {
    _id: string,
    displayName: string,
  },
  track: {
    _id: string,
    title: string,
    length: string,
    album: string
  },
  artist: {
    _id: string,
    name: string
  }
  datetime: string
}

export interface Track {
  _id: string,
  title: string,
  length: string,
  is_published: boolean,
  album: {
    _id: string,
    title: string
  }
}

export interface TrackData {
  title: string;
  album: string;
  length: string;
  is_published: boolean;
}

export class Artist {
  constructor(
    public id: string,
    public name: string,
    public information: string,
    public is_published: boolean,
    public image: string
  ) {}
}

export class Album {
  constructor(
    public id: string,
    public artist: Object,
    public title: string,
    public date: string,
    public is_published: boolean,
    public image: string,
  ) {}
}

export interface AlbumData {
  [key: string]: any;
  artist: string;
  title: string;
  date: string;
  image: File;
}

export interface ArtistData {
  [key: string]: any;
  name: string;
  information: string;
  image: File;
}

export interface ApiAlbumData {
  _id: string,
  artist: {
    _id: string,
    name: string
  },
  title: string,
  date: string,
  is_published: boolean,
  image: string
}

export interface AlbumToCreate {
  albums: ApiAlbumData[],
  artist: null
}

export interface DataApi {
  albums : ApiAlbumData[],
  artist: {
    _id: string,
    name: string,
    information: string,
    is_published: boolean,
    image: string
  }
}

export interface ApiArtistData {
  _id: string,
  name: string,
  information: string,
  is_published: boolean,
  image: string
}

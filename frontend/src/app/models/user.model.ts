export interface User {
  _id: string,
  email: string,
  avatar: string,
  displayName: string,
  token: string,
  role: string
}

export interface LoginFacebookUser {
  id: string,
  authToken: string,
  email: string,
  name: string,
  avatar: string
}

export interface RegisterUserData {
  email: string,
  password: string
}

export interface LoginUserData {
  email: string,
  password: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface LoginError {
  error: string
}

export interface FieldError {
  message: string
}

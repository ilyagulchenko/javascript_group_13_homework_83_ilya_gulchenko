import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs';
import {ArtistsService} from '../services/artists.service';
import {fetchDataRequest, fetchDataSuccess} from './data.actions';

@Injectable()
export class DataEffects {
  fetchData = createEffect(() => this.actions.pipe(
    ofType(fetchDataRequest),
    mergeMap(({id}) => this.artistsService.getArtistById(id).pipe(
      map(data => fetchDataSuccess({data}))
    ))
  ));

  constructor(
    private actions: Actions,
    private artistsService: ArtistsService,
  ) {}
}

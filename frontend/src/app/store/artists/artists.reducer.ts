import { createReducer, on } from '@ngrx/store';
import {
  changeArtistRequest, changeArtistSuccess,
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess, removeArtistFailure, removeArtistRequest, removeArtistSuccess
} from './artists.actions';
import { ArtistsState } from '../types';

const initialState: ArtistsState = {
  artists: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  changeLoading: false,
  changeError: null,
  deleteLoading: false,
  deleteError: null,
}

export const artistsReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistsSuccess, (state, {artists}) => ({...state, fetchLoading: false, artists})),
  on(fetchArtistsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createArtistRequest, state => ({...state, createLoading: true})),
  on(createArtistSuccess, state => ({...state, createLoading: false})),
  on(createArtistFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(changeArtistRequest, state => ({...state, changeLoading: true})),
  on(changeArtistSuccess, (state, {artist}) => ({
    ...state,
    artists: state.artists.map(item => {
      if (item.id === artist.id) {
        return artist;
      }
      return item;
    })
  })),

  on(removeArtistRequest, state => ({...state, deleteLoading: true})),
  on(removeArtistSuccess, state => ({...state, deleteLoading: false})),
  on(removeArtistFailure, (state, {error}) => ({...state, deleteLoading: false, deleteError: error})),
);

import { createAction, props } from '@ngrx/store';
import {Artist, ArtistData} from '../../models/artist.model';

export const fetchArtistsRequest = createAction('[Artists] Fetch Request');
export const fetchArtistsSuccess = createAction('[Artists] Fetch Success', props<{artists: Artist[]}>());
export const fetchArtistsFailure = createAction('[Artists] Fetch Failure', props<{error: string}>());

export const createArtistRequest = createAction('[Artist] Create Request', props<{artistData: ArtistData}>());
export const createArtistSuccess = createAction('[Artist] Create Success');
export const createArtistFailure = createAction('[Artist] Create Failure', props<{error: string}>());

export const changeArtistRequest = createAction('[Artist] Change Request', props<{id: string, change: {is_published: true}}>());
export const changeArtistSuccess = createAction('[Artist] Change Success', props<{artist: Artist}>());
export const changeArtistFailure = createAction('[Artist] Change Failure', props<{error: string}>());

export const removeArtistRequest = createAction('[Artist] Remove Request', props<{id: string}>());
export const removeArtistSuccess = createAction('[Artist] Remove Success');
export const removeArtistFailure = createAction('[Artist] Remove Failure', props<{error: string}>());

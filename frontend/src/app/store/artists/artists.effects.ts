import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  changeArtistRequest, changeArtistSuccess,
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess, removeArtistFailure, removeArtistRequest, removeArtistSuccess
} from './artists.actions';
import {catchError, map, mergeMap, of, tap} from 'rxjs';
import { ArtistsService } from '../../services/artists.service';
import {Router} from '@angular/router';

@Injectable()
export class ArtistsEffects {

  constructor(
    private actions: Actions,
    private artistsService: ArtistsService,
    private router: Router
  ) {}

  fetchArtists = createEffect(() => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.artistsService.getArtists().pipe(
      map( artists => fetchArtistsSuccess({artists})),
      catchError(() => of(fetchArtistsFailure({error: 'Something wrong'})))
    ))
  ));

  createProduct = createEffect(() => this.actions.pipe(
    ofType(createArtistRequest),
    mergeMap(({artistData}) => this.artistsService.createArtist(artistData).pipe(
      map(() => createArtistSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createArtistFailure({error: 'Wrong data'})))
    ))
  ));

  changeArtist = createEffect(() => this.actions.pipe(
    ofType(changeArtistRequest),
    mergeMap(({id, change}) => this.artistsService.publishArtist(id, change).pipe(
      map(artist => changeArtistSuccess({artist}))
    ))
  ));

  removeArtist = createEffect(() => this.actions.pipe(
    ofType(removeArtistRequest),
    mergeMap(({id}) => this.artistsService.removeArtist(id).pipe(
      map(() => removeArtistSuccess()),
      catchError(() => of(removeArtistFailure({error: 'Something wrong'})))
    ))
  ));
}

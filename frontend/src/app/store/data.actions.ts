import {createAction, props} from '@ngrx/store';
import {DataApi} from '../models/artist.model';

export const fetchDataRequest = createAction('[Data] Fetch Request', props<{id: string}>());
export const fetchDataSuccess = createAction('[Data] Fetch Success', props<{data: DataApi}>());

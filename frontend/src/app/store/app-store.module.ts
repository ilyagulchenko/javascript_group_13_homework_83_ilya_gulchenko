import {ActionReducer, MetaReducer, StoreModule} from '@ngrx/store';
import {localStorageSync} from 'ngrx-store-localstorage';
import {NgModule} from '@angular/core';
import {ArtistsEffects} from './artists/artists.effects';
import {UsersEffects} from './users/users.effects';
import {DataEffects} from './data.effects';
import {TracksEffects} from './tracks/tracks.effects';
import {TrackHistoryEffects} from './track-histories/trackHistory.effects';
import {artistsReducer} from './artists/artists.reducer';
import {usersReducer} from './users/users.reducer';
import {dataReducer} from './data.reducer';
import {trackReducer} from './tracks/tracks.reducer';
import {trackHistoryReducer} from './track-histories/trackHistory.reducer';
import {EffectsModule} from '@ngrx/effects';
import {albumsReducer} from './albums/albums.reducer';
import {AlbumsEffects} from './albums/albums.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  artists: artistsReducer,
  albums: albumsReducer,
  users: usersReducer,
  data: dataReducer,
  tracks: trackReducer,
  trackHistory: trackHistoryReducer
}

const effects = [ArtistsEffects, AlbumsEffects, UsersEffects, DataEffects, TracksEffects, TrackHistoryEffects]

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ]
})
export class AppStoreModule {}

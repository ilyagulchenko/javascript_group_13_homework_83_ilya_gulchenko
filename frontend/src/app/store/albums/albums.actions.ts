import {createAction, props} from '@ngrx/store';
import {AlbumData, AlbumToCreate} from '../../models/artist.model';

export const fetchAlbumsRequest = createAction('[Albums] Fetch Request');
export const fetchAlbumsSuccess = createAction('[Albums] Fetch Success', props<{albums: AlbumToCreate}>());
export const fetchAlbumsFailure = createAction('[Albums] Fetch Failure', props<{error: string}>());

export const createAlbumRequest = createAction('[Albums] Create Request', props<{albumData: AlbumData}>());
export const createAlbumSuccess = createAction('[Albums] Create Success');
export const createAlbumFailure = createAction('[Albums] Create Failure', props<{error: string}>());

export const removeAlbumRequest = createAction('[Albums] Remove Request', props<{id: string}>());
export const removeAlbumSuccess = createAction('[Albums] Remove Success');
export const removeAlbumFailure = createAction('[Albums] Remove Failure', props<{error: string}>());

import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {catchError, map, mergeMap, of, tap} from 'rxjs';
import {
  createAlbumFailure,
  createAlbumRequest,
  createAlbumSuccess, fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess, removeAlbumFailure, removeAlbumRequest, removeAlbumSuccess
} from './albums.actions';
import {AlbumsService} from '../../services/albums.service';
import {Router} from '@angular/router';

@Injectable()
export class AlbumsEffects {

  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
    private router: Router
  ) {}

  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(() => this.albumsService.getAlbums().pipe(
      map( albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({error: 'Something wrong'})))
    ))
  ));

  createAlbum = createEffect(() => this.actions.pipe(
    ofType(createAlbumRequest),
    mergeMap(({albumData}) => this.albumsService.createAlbum(albumData).pipe(
      map(() => createAlbumSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createAlbumFailure({error: 'Wrong data'})))
    ))
  ));

  removeAlbum = createEffect(() => this.actions.pipe(
    ofType(removeAlbumRequest),
    mergeMap(({id}) => this.albumsService.removeAlbum(id).pipe(
      map(() => removeAlbumSuccess()),
      catchError(() => of(removeAlbumFailure({error: 'Something wrong'})))
    ))
  ));
}

import {AlbumsState} from '../types';
import {createReducer, on} from '@ngrx/store';
import {
  createAlbumFailure,
  createAlbumRequest,
  createAlbumSuccess, fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess, removeAlbumFailure, removeAlbumRequest, removeAlbumSuccess
} from './albums.actions';

const initialState: AlbumsState = {
  albums: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  deleteLoading: false,
  deleteError: null,
}

export const albumsReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumsSuccess, (state, {albums}) => ({...state, fetchLoading: false, albums})),
  on(fetchAlbumsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createAlbumRequest, state => ({...state, createLoading: true})),
  on(createAlbumSuccess, state => ({...state, createLoading: false})),
  on(createAlbumFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(removeAlbumRequest, state => ({...state, deleteLoading: true})),
  on(removeAlbumSuccess, state => ({...state, deleteLoading: false})),
  on(removeAlbumFailure, (state, {error}) => ({...state, deleteLoading: false, deleteError: error})),
);

import {AlbumToCreate, Artist, DataApi} from '../models/artist.model';
import {LoginError, RegisterError, User} from '../models/user.model';
import {Track} from '../models/track.model';
import {TrackHistory} from '../models/trackHistory.model';

export type ArtistsState = {
  artists:  Artist[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  changeLoading: boolean,
  changeError: null | string,
  deleteLoading: boolean,
  deleteError: null | string,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
};

export type DataState = {
  data: null | DataApi,
  fetchLoading: boolean,
}

export type TracksState = {
  tracks: Track[],
  fetchLoading: boolean,
  createLoading: boolean,
  createError: null | string,
  changeLoading: boolean,
  changeError: null | string,
  deleteLoading: boolean,
  deleteError: null | string,
}

export type TrackHistoryState = {
  trackHistory: TrackHistory[],
  fetchLoading: boolean,
  fetchError: null | string,
}

export type AlbumsState = {
  albums:  null | AlbumToCreate,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  deleteLoading: boolean,
  deleteError: null | string,
};

export type AppState = {
  artists: ArtistsState,
  albums: AlbumsState,
  users: UsersState,
  data: DataState,
  tracks: TracksState,
  trackHistory: TrackHistoryState
};

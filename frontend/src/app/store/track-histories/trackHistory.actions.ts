import {createAction, props} from '@ngrx/store';
import {TrackHistory} from '../../models/trackHistory.model';

export const fetchTrackHistoryRequest = createAction('[TrackHistory] Fetch Request');
export const fetchTrackHistorySuccess = createAction('[TrackHistory] Fetch Success', props<{trackHistory: TrackHistory[]}>());
export const fetchTrackHistoryFailure = createAction('[TrackHistory] Fetch Failure', props<{error: string}>());

export const createTrackHistoryRequest = createAction('[TrackHistory] Create Request', props<{id: string}>());
export const createTrackHistorySuccess = createAction('[TrackHistory] Create Success');

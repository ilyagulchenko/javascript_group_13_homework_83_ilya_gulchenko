import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  createTrackHistoryRequest,
  createTrackHistorySuccess,
  fetchTrackHistoryFailure,
  fetchTrackHistoryRequest,
  fetchTrackHistorySuccess
} from './trackHistory.actions';
import {catchError, map, mergeMap, NEVER, of, withLatestFrom} from 'rxjs';
import {TrackHistoryService} from '../../services/trackHistory.service';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../types';

@Injectable()
export class TrackHistoryEffects {
  createTrackHistory = createEffect(() => this.actions.pipe(
    ofType(createTrackHistoryRequest),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([id, user]) => {
      if (user) {
        return this.trackHistoryService.addTrackToHistory({id}, user.token).pipe(
          map(() => createTrackHistorySuccess())
        );
      }

      return NEVER;
    })
  ));

  fetchTrackHistory = createEffect(() => this.actions.pipe(
    ofType(fetchTrackHistoryRequest),
    mergeMap(() => this.trackHistoryService.fetchTrackHistory().pipe(
      map( trackHistory => fetchTrackHistorySuccess({trackHistory})),
      catchError(() => of(fetchTrackHistoryFailure({error: 'Something wrong'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private trackHistoryService: TrackHistoryService,
    private router: Router,
    private store: Store<AppState>
  ) {}
}

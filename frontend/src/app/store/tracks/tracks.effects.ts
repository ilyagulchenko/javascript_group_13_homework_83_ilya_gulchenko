import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap, of, tap} from 'rxjs';
import {
  changeTrackRequest,
  changeTrackSuccess,
  createTrackFailure,
  createTrackRequest,
  createTrackSuccess,
  fetchTracksRequest,
  fetchTracksSuccess,
  removeTrackFailure,
  removeTrackRequest,
  removeTrackSuccess
} from './tracks.actions';
import {TracksService} from '../../services/tracks.service';
import {Router} from '@angular/router';

@Injectable()
export class TracksEffects {

  constructor(
    private actions: Actions,
    private trackService: TracksService,
    private router: Router
  ) {}

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(({id}) => this.trackService.getTracks(id).pipe(
      map(tracks => fetchTracksSuccess({tracks}))
    ))
  ));

  createTrack = createEffect(() => this.actions.pipe(
    ofType(createTrackRequest),
    mergeMap(({trackData}) => this.trackService.createTrack(trackData).pipe(
      map(() => createTrackSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createTrackFailure({error: 'Wrong data'})))
    ))
  ));

  changeTrack = createEffect(() => this.actions.pipe(
    ofType(changeTrackRequest),
    mergeMap(({id, change}) => this.trackService.publishTrack(id, change).pipe(
      map(track => changeTrackSuccess({track}))
    ))
  ));

  removeTrack = createEffect(() => this.actions.pipe(
    ofType(removeTrackRequest),
    mergeMap(({id}) => this.trackService.removeTrack(id).pipe(
      map(() => removeTrackSuccess()),
      catchError(() => of(removeTrackFailure({error: 'Something wrong'})))
    ))
  ));
}

import {createAction, props} from '@ngrx/store';
import {Track, TrackData} from '../../models/track.model';

export const fetchTracksRequest = createAction('[Track] Fetch Request', props<{id: string}>());
export const fetchTracksSuccess = createAction('[Track] Fetch Success', props<{tracks: Track[]}>());

export const createTrackRequest = createAction('[Track] Create Request', props<{trackData: TrackData}>());
export const createTrackSuccess = createAction('[Track] Create Success');
export const createTrackFailure = createAction('[Track] Create Failure', props<{error: string}>());

export const changeTrackRequest = createAction('[Track] Change Request', props<{id: string, change: {is_published: true}}>());
export const changeTrackSuccess = createAction('[Track] Change Success', props<{track: Track}>());
export const changeTrackFailure = createAction('[Track] Change Failure', props<{error: string}>());

export const removeTrackRequest = createAction('[Track] Remove Request', props<{id: string}>());
export const removeTrackSuccess = createAction('[Track] Remove Success');
export const removeTrackFailure = createAction('[Track] Remove Failure', props<{error: string}>());

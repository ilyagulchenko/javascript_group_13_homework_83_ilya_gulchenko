import {TracksState} from '../types';
import {createReducer, on} from '@ngrx/store';
import {
  changeTrackRequest,
  changeTrackSuccess,
  createTrackFailure,
  createTrackRequest,
  createTrackSuccess,
  fetchTracksRequest,
  fetchTracksSuccess,
  removeTrackFailure,
  removeTrackRequest,
  removeTrackSuccess
} from './tracks.actions';

const initialState: TracksState = {
  tracks: [],
  fetchLoading: false,
  createLoading: false,
  createError: null,
  changeLoading: false,
  changeError: null,
  deleteLoading: false,
  deleteError: null,
}

export const trackReducer = createReducer(
  initialState,
  on(fetchTracksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTracksSuccess, (state, {tracks}) => ({...state, fetchLoading: false, tracks})),

  on(createTrackRequest, state => ({...state, createLoading: true})),
  on(createTrackSuccess, state => ({...state, createLoading: false})),
  on(createTrackFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(changeTrackRequest, state => ({...state, changeLoading: true})),
  on(changeTrackSuccess, (state, {track}) => ({
    ...state,
    tracks: state.tracks.map(item => {
      if (item._id === track._id) {
        return track;
      }
      return item;
    })
  })),

  on(removeTrackRequest, state => ({...state, deleteLoading: true})),
  on(removeTrackSuccess, state => ({...state, deleteLoading: false})),
  on(removeTrackFailure, (state, {error}) => ({...state, deleteLoading: false, deleteError: error})),
);
